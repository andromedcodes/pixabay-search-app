package com.github.andromedcodes.pixabay.utils

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.BindingAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */

@BindingAdapter("loadingVisibility")
fun setLoadingVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null)
        visibility.observe(parentActivity, Observer { value ->
            view.visibility = value ?: View.VISIBLE
        })
}

@BindingAdapter("textValue")
fun setTextValue(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null)
        text.observe(parentActivity, Observer { value ->
            view.text = value ?: ""
        })
}

@BindingAdapter("textWatcher")
fun setTextWatcher(view: EditText, listener: MutableLiveData<TextWatcher>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && listener != null)
        listener.observe(parentActivity, Observer {
            view.addTextChangedListener(it)
        })

}

@BindingAdapter("mediaUrl")
fun setMediaUrl(view: ImageView, url: MutableLiveData<String>?) {
    val parentActivity = view.getParentActivity()
    if (parentActivity != null && url != null)
        url.observe(parentActivity, Observer {
            Glide.with(view)
                .asBitmap()
                .load(it)
                .into(view)
        })
}

@BindingAdapter("scrollListener")
fun setScrollListener(view: RecyclerView, listener: MutableLiveData<RecyclerView.OnScrollListener>?) {
    val parentActivity = view.getParentActivity()
    if (parentActivity != null && listener != null)
        listener.observe(parentActivity, Observer {
            view.addOnScrollListener(it!!)
        })
}