package com.github.andromedcodes.pixabay.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
data class SearchResponse(
    @SerializedName("total") val total: Int,
    @SerializedName("totalHits") val totalHits: Int,
    @SerializedName("hits") val result: List<Media>?
)