package com.github.andromedcodes.pixabay

import android.app.Application

/**
 * Created by mohamed on 27/11/2018.
 * email: devmed01@gmail.com
 */
class PixabayApp : Application() {

    companion object {
        lateinit var instance: PixabayApp
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}