package com.github.andromedcodes.pixabay.data

import com.github.andromedcodes.pixabay.BuildConfig
import com.github.andromedcodes.pixabay.data.model.SearchResponse
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
class MediaRepository @Inject constructor(private val api: PixabayApi) {

    fun search(query: String, page: Int, perPage: Int): Observable<SearchResponse> =
        api.lookup(BuildConfig.API_KEY, query, page, perPage)
}