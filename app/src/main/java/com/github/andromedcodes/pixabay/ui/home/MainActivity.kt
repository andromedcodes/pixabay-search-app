package com.github.andromedcodes.pixabay.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.github.andromedcodes.pixabay.R
import com.github.andromedcodes.pixabay.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MediaListViewModel
    private var errorSnackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // initiate data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // assign recyclerView Layout Manager through data-binding
        binding.searchResults.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        // get MediaListViewModel instance
        viewModel = ViewModelProviders.of(this)
            .get(MediaListViewModel::class.java)

        // Observe viewModel error message changes
        viewModel.errorMessage.observe(this, Observer {
            // show error message if available
                errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })

        // bind view model (MediaListViewModel) instance into activity binding instance
        binding.viewModel = viewModel

    }

    /**
     * In case of loading error, show SnackBack with retry action
     * @param: errorMessage: snackBack error message
     */
    private fun showError(@StringRes errorMessage: Int) {
        errorSnackBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackBar?.setAction(R.string.action_retry_title, viewModel.errorClickListener)
        errorSnackBar?.show()
    }

    /**
     * dismiss snackBar
     */
    private fun hideError() {
        errorSnackBar?.dismiss()
    }
}
