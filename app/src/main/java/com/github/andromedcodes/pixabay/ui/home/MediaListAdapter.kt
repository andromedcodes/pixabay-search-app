package com.github.andromedcodes.pixabay.ui.home

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.andromedcodes.pixabay.R
import com.github.andromedcodes.pixabay.data.model.Media
import com.github.andromedcodes.pixabay.databinding.SearchListItemBinding
import com.github.andromedcodes.pixabay.utils.getParentActivity

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
class MediaListAdapter :
    RecyclerView.Adapter<MediaListAdapter.MediaViewHolder>() {

    private lateinit var mediaList: MutableList<Media>

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MediaViewHolder {
        val binding: SearchListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.search_list_item,
            parent,
            false
        )
        return MediaViewHolder(binding)
    }

    override fun getItemCount(): Int = if (::mediaList.isInitialized) mediaList.size else 0


    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) =
        holder.bind(mediaList[position])

    fun updateList(result: List<Media>, newList: Boolean) {
        if (newList) {
            mediaList = result as MutableList<Media>
            notifyDataSetChanged()
        } else {
            mediaList.addAll(result)
            notifyItemRangeInserted(mediaList.size - result.size, mediaList.size)
        }

    }

    class MediaViewHolder(private val binding: SearchListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = MediaViewModel()

        fun bind(media: Media) {
            viewModel.bindData(media)
            binding.root.setOnClickListener { view ->
                AlertDialog.Builder(view?.getParentActivity() as Context)
                    .setTitle(R.string.item_clicked_dialog_title)
                    .setMessage(R.string.view_item_details_dialog_message)
                    .setPositiveButton(R.string.view_item_dialog_positive_answer)
                    { _, _ ->
                        // todo: use event bus handler to dispatch item click event and listen to it from the (fragment/ activity)
                        Log.e("tag", "dispatch event")
                    }
                    .setNegativeButton(
                        R.string.view_item_dialog_negative_answer
                    ) { dialog, p1 -> dialog?.dismiss() }
                    .create()
                    .show()
            }
            binding.viewModel = viewModel
        }
    }
}