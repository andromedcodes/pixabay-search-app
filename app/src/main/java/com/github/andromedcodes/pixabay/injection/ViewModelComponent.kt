package com.github.andromedcodes.pixabay.injection

import com.github.andromedcodes.pixabay.injection.modules.DataModule
import com.github.andromedcodes.pixabay.injection.modules.NetworkModule
import com.github.andromedcodes.pixabay.ui.home.MediaListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
@Singleton
@Component(
    modules = [DataModule::class, NetworkModule::class]
)
interface ViewModelComponent {

    /**
     * inject required dependencies into MediaListViewModel
     */
    fun inject(viewModel: MediaListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelComponent

        fun bindDataModule(dataModule: DataModule): Builder

        fun bindNetworkModule(networkModule: NetworkModule): Builder
    }

}