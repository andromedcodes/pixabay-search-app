package com.github.andromedcodes.pixabay.injection.modules

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
@Module
object DataModule {

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideGson() = GsonBuilder()
        .setLenient()
        .serializeNulls()
        .create()
}