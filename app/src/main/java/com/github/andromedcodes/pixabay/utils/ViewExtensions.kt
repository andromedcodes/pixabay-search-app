package com.github.andromedcodes.pixabay.utils

import android.content.Context
import android.content.ContextWrapper
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */

fun View.getParentActivity(): AppCompatActivity? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }
        context = context.baseContext
    }
    return null
}