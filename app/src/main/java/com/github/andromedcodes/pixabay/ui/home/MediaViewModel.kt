package com.github.andromedcodes.pixabay.ui.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.github.andromedcodes.pixabay.data.model.Media

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
class MediaViewModel : ViewModel() {
    private val authorName = MutableLiveData<String>()
    private val mediaUrl = MutableLiveData<String>()
    private val itemTags = MutableLiveData<String>()
    private val selected = MutableLiveData<Boolean>()

    /**
     * bind Media item data into view model fields
     */
    fun bindData(media: Media) {
        authorName.value = media.user
        mediaUrl.value = media.previewURL
        itemTags.value = media.tags
    }

    /**
     * [authorName] getter
     */
    fun getAuthorName(): MutableLiveData<String> = authorName

    /**
     * [mediaUrl] getter
     */
    fun getMediaUrl(): MutableLiveData<String> = mediaUrl

    /**
     * [itemTags] getter
     */
    fun getItemTags(): MutableLiveData<String> = itemTags

    fun getSelected(): MutableLiveData<Boolean> = selected
}