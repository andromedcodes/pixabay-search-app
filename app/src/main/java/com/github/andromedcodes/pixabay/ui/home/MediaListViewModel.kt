package com.github.andromedcodes.pixabay.ui.home

import android.arch.lifecycle.MutableLiveData
import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.github.andromedcodes.pixabay.data.MediaRepository
import com.github.andromedcodes.pixabay.data.model.Media
import com.github.andromedcodes.pixabay.data.model.SearchResponse
import com.github.andromedcodes.pixabay.ui.base.BaseViewModel
import com.github.andromedcodes.pixabay.utils.decodeAsHttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
class MediaListViewModel : BaseViewModel() {

    @Inject
    lateinit var repository: MediaRepository
    private lateinit var searchQuery: String
    private var currentPage = 1 // the current result page
    private var isNewQuery = true // flag to decide resetting the list or adding new items (pagination)

    val loaderVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener: View.OnClickListener =
        View.OnClickListener {
            loadSearchResult(searchQuery, currentPage, 20)
        }
    val mediaListAdapter = MediaListAdapter()
    val scrollListener = MutableLiveData<RecyclerView.OnScrollListener>()
    val textWatcher = MutableLiveData<TextWatcher>()

    init {
        scrollListener.value = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                // Load next page of exists
                if (!recyclerView.canScrollVertically(1)) {
                    isNewQuery = false
                    loadSearchResult(searchQuery, ++currentPage, 20)
                }
            }
        }

        textWatcher.value = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // Initiate fetching result only when the user inserts a new word
                if (s.length > 1 && s[s.length - 1].toString() == " ") {
                    searchQuery = s.toString()
                    currentPage = 1
                    isNewQuery = true
                    loadSearchResult(searchQuery, currentPage, 20)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        }
        searchQuery = "flowers"
        loadSearchResult(searchQuery, currentPage, 20)
    }

    /**
     * hits the repository to fetch data based on user query and current data position (currentPage)
     */
    private fun loadSearchResult(query: String, page: Int, perPage: Int) {
        subscription = repository.search(query, page, perPage)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onSearchInitiated() }
            .doOnTerminate { onSearchTerminated() }
            .subscribe(
                { result -> onSearchSuccess(result.result) },
                { error -> onSearchFailure(error.decodeAsHttpException()) }
            )
    }

    /**
     * prepare view to render loading status
     */
    private fun onSearchInitiated() {
        loaderVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    /**
     * prepare view to render terminated loading status
     */
    private fun onSearchTerminated() {
        loaderVisibility.value = View.GONE
    }

    /**
     * react to successful result loading
     */
    private fun onSearchSuccess(resultList: List<Media>?) {
        resultList.let { list ->
            mediaListAdapter.updateList(list!!, isNewQuery)
        }
    }

    /**
     * react to failed result loading
     */
    private fun onSearchFailure(error: Int) {
        errorMessage.value = error
    }

}