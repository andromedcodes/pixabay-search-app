package com.github.andromedcodes.pixabay.data

import com.github.andromedcodes.pixabay.data.model.SearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
interface PixabayApi {

    @GET("./")
    fun lookup(
        @Query("key") apiKey: String,
        @Query("q") searchQuery: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Observable<SearchResponse>
}