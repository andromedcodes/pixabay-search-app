package com.github.andromedcodes.pixabay.ui.base

import android.arch.lifecycle.ViewModel
import com.github.andromedcodes.pixabay.injection.DaggerViewModelComponent
import com.github.andromedcodes.pixabay.injection.ViewModelComponent
import com.github.andromedcodes.pixabay.injection.modules.DataModule
import com.github.andromedcodes.pixabay.injection.modules.NetworkModule
import com.github.andromedcodes.pixabay.ui.home.MediaListViewModel
import io.reactivex.disposables.Disposable

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */
abstract class BaseViewModel : ViewModel() {

    lateinit var subscription: Disposable

    private val injector: ViewModelComponent =
        DaggerViewModelComponent.builder()
            .bindDataModule(DataModule)
            .bindNetworkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is MediaListViewModel -> injector.inject(this)
        }
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}