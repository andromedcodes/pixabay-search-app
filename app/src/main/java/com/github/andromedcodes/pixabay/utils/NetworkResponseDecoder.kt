package com.github.andromedcodes.pixabay.utils

import com.github.andromedcodes.pixabay.R
import retrofit2.HttpException

/**
 * Created by mohamed on 26/11/2018.
 * email: devmed01@gmail.com
 */

/**
 * Decode the error if it's a HttpException and return
 * a string resource based on the error status code
 * @return : Mapped error message as string resource
 */
fun Throwable.decodeAsHttpException(): Int =
    (this as? HttpException)?.let { httpException ->
        when (httpException.code()) {
            400 -> R.string.error_bad_request
            500 -> R.string.error_server_error
            429 -> R.string.error_too_many_requests
            else -> R.string.search_error_message
        }
    } ?: R.string.search_error_message
